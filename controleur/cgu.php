<?php

use modele\dao\Bdd;

/**
 * Contrôleur cgu
 * Page des Conditions Générales d'Utilisation
 *
 * Vue contrôlée : vueCgu
 */

// Récupération des données GET, POST, et SESSION
//
// Récupération des données utilisées dans la vue
Bdd::connecter();

$titre = "r3st0.fr - Conditions générales d'utilisations";

// creation du menu burger
$menuBurger = [];
$menuBurger[] = ["url"=>"#top","label"=>"Conditions générales"];
$menuBurger[] = ["url"=>"#accpt","label"=>"Acceptation"];
$menuBurger[] = ["url"=>"#desc","label"=>"Description"];
$menuBurger[] = ["url"=>"#fonc","label"=>"Fonctionnalités"];
$menuBurger[] = ["url"=>"#mode","label"=>"Modération"];
$menuBurger[] = ["url"=>"#sanc","label"=>"Sanctions"];
$menuBurger[] = ["url"=>"#moti","label"=>"Motifs"];
$menuBurger[] = ["url"=>"#foncr","label"=>"Restaurateurs"];
$menuBurger[] = ["url"=>"#gene","label"=>"Généralités"];
$menuBurger[] = ["url"=>"#prot","label"=>"Données personnelles"];
$menuBurger[] = ["url"=>"#droi","label"=>"Droits d'accès"];
$menuBurger[] = ["url"=>"#util","label"=>"Données personnelles"];
$menuBurger[] = ["url"=>"#bila","label"=>"Bilan des fonctionnalités"];



// Construction de la vue
require_once "$racine/vue/entete.html.php";
require_once "$racine/vue/vueCgu.php";
require_once "$racine/vue/pied.html.php";

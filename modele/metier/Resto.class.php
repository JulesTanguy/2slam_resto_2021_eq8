<?php

namespace modele\metier;

/**
 * Description of Resto
 *
 * @author N. Bourgeois
 * @version 07/2021
 */
class Resto
{
    /** @var int identifiant du restaurant */
    private int $idR;
    /** @var string nom du restaurant */
    private ?string $nomR;
    /** @var string adresse : n° dans la voie */
    private ?string $numAdr;
    /** @var string adresse : nom de la voie */
    private ?string $voieAdr;
    /** @var string adresse : code postal */
    private ?string $cpR;
    /** @var string adresse : nom de la ville */
    private ?string $villeR;
    /** @var float position : latitude en degrés */
    private ?float $latitudeDegR;
    /** @var float position : longitude en degrés */
    private ?float $longitudeDegR;
    /** @var string description du restaurant */
    private ?string $descR;
    /** @var string texte HTML d'affichage des horaires d'ouverture */
    private ?string $horairesR;
    /** @var array() tableau d'objets Photo : les photos du restaurant ; la photo principale en 1ère position */
    private array $lesPhotos;
    /** @var array() tableau d'objets Critique : les notes et commentaires déposées par les utilisateurs sur le restaurant */
    private array $lesCritiques;
    /** @var array() tableau d'objets TypesCuisine : les types de cuisine proposés par le restaurant */
    private array $lesTypesCuisineProposes;

    private ?string $semaineMidi;

    private ?string $semaineSoir;

    private ?string $weekMidi;

    private ?string $weekSoir;

    private ?string $emporterSemaine;

    private ?string $emporterWeek;

    public function __construct(int $idR, string $nomR, ?string $numAdr, ?string $voieAdr, ?string $cpR, ?string $villeR, ?float $latitudeDegR, ?float $longitudeDegR, ?string $descR, ?string $semaineMidi, ?string $semaineSoir, ?string $weekMidi, ?string $weekSoir, ?string $emporterSemaine, ?string $emporterWeek)
    {
        $this->idR = $idR;
        $this->nomR = $nomR;
        $this->numAdr = $numAdr;
        $this->voieAdr = $voieAdr;
        $this->cpR = $cpR;
        $this->villeR = $villeR;
        $this->latitudeDegR = $latitudeDegR;
        $this->longitudeDegR = $longitudeDegR;
        $this->descR = $descR;
        $this->lesCritiques = [];
        $this->lesPhotos = [];
        $this->lesTypesCuisineProposes = [];
        $this->semaineMidi = $semaineMidi;
        $this->semaineSoir = $semaineSoir;
        $this->weekMidi = $weekMidi;
        $this->weekSoir = $weekSoir;
        $this->emporterSemaine = $emporterSemaine;
        $this->emporterWeek = $emporterWeek;
    }

    public function getSemaineMidi(): ?string
    {
        return $this->semaineMidi;
    }

    public function getSemaineSoir(): ?string
    {
        return $this->semaineSoir;
    }

    public function getWeekMidi(): ?string
    {
        return $this->weekMidi;
    }

    public function getWeekSoir(): ?string
    {
        return $this->weekSoir;
    }

    public function getEmporterSemaine(): ?string
    {
        return $this->emporterSemaine;
    }

    public function getEmporterWeek(): ?string
    {
        return $this->emporterWeek;
    }

    public function setSemaineMidi(?string $SemaineMidi): void
    {
        $this->semaineMidi = $SemaineMidi;
    }

    public function setSemaineSoir(?string $SemaineSoir): void
    {
        $this->semaineSoir = $SemaineSoir;
    }

    public function setWeekMidi(?string $weekMidi): void
    {
        $this->weekMidi = $weekMidi;
    }

    public function setWeekSoir(?string $weekSoir): void
    {
        $this->weekSoir = $weekSoir;
    }

    public function setEmporterSemaine(?string $emporterSemaine): void
    {
        $this->emporterSemaine = $emporterSemaine;
    }

    public function setEmporterWeek(?string $emporterWeek): void
    {
        $this->emporterWeek = $emporterWeek;
    }


    public function getIdR(): int
    {
        return $this->idR;
    }

    public function getNomR(): ?string
    {
        return $this->nomR;
    }

    public function getNumAdr(): ?string
    {
        return $this->numAdr;
    }

    public function getVoieAdr(): ?string
    {
        return $this->voieAdr;
    }

    public function getCpR(): ?string
    {
        return $this->cpR;
    }

    public function getVilleR(): ?string
    {
        return $this->villeR;
    }

    public function getLatitudeDegR(): ?float
    {
        return $this->latitudeDegR;
    }

    public function getLongitudeDegR(): ?float
    {
        return $this->longitudeDegR;
    }

    public function getDescR(): ?string
    {
        return $this->descR;
    }


    public function getLesPhotos(): array
    {
        return $this->lesPhotos;
    }

    public function getLesCritiques(): array
    {
        return $this->lesCritiques;
    }

    public function getLesTypesCuisineProposes(): array
    {
        return $this->lesTypesCuisineProposes;
    }

    public function setIdR(int $idR): void
    {
        $this->idR = $idR;
    }

    public function setNomR(string $nomR): void
    {
        $this->nomR = $nomR;
    }

    public function setNumAdr(string $numAdr): void
    {
        $this->numAdr = $numAdr;
    }

    public function setVoieAdr(string $voieAdr): void
    {
        $this->voieAdr = $voieAdr;
    }

    public function setCpR(string $cpR): void
    {
        $this->cpR = $cpR;
    }

    public function setVilleR(string $villeR): void
    {
        $this->villeR = $villeR;
    }

    public function setLatitudeDegR(float $latitudeDegR): void
    {
        $this->latitudeDegR = $latitudeDegR;
    }

    public function setLongitudeDegR(float $longitudeDegR): void
    {
        $this->longitudeDegR = $longitudeDegR;
    }

    public function setDescR(string $descR): void
    {
        $this->descR = $descR;
    }

    public function setLesPhotos(array $lesPhotos): void
    {
        $this->lesPhotos = $lesPhotos;
    }

    public function setLesCritiques(array $lesCritiques): void
    {
        $this->lesCritiques = $lesCritiques;
    }

    public function setLesTypesCuisineProposes(array $lesTypesCuisineProposes): void
    {
        $this->lesTypesCuisineProposes = $lesTypesCuisineProposes;
    }
}

<?php

namespace modele\metier;

/**
 * Description of Photo
 *
 * @author N. Bourgeois
 * @version 07/2021
 */
class Photo
{
    private int $idP;
    private string $cheminP;

    public function __construct(int $idP, string $cheminP)
    {
        $this->idP = $idP;
        $this->cheminP = $cheminP;
    }
    public function getIdP(): int
    {
        return $this->idP;
    }

    public function getCheminP(): string
    {
        return $this->cheminP;
    }

    public function setIdP(int $idP): void
    {
        $this->idP = $idP;
    }

    public function setCheminP(string $cheminP): void
    {
        $this->cheminP = $cheminP;
    }
}

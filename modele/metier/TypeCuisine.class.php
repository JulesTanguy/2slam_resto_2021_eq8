<?php

namespace modele\metier;

/**
 * Description of TypeCuisine
 *
 * @author N. Bourgeois
 */

class TypeCuisine
{
    private int $idTC;
    private string $libelleTC;

    public function __construct(int $idTC, string $libelleTC)
    {
        $this->idTC = $idTC;
        $this->libelleTC = $libelleTC;
    }

    public function getIdTC(): int
    {
        return $this->idTC;
    }

    public function getLibelleTC(): string
    {
        return $this->libelleTC;
    }

    public function setIdTC(int $idTC): void
    {
        $this->idTC = $idTC;
    }

    public function setLibelleTC(string $libelleTC): void
    {
        $this->libelleTC = $libelleTC;
    }
}
